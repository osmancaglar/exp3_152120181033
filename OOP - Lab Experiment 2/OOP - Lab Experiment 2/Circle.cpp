/*deneme
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
//namespace Circle {
#define PI 3.14
Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r; 
}

void Circle::setR(int r) {
	this->r = r;
}

double Circle::getR() const{
	return r;
}

double Circle::calculateCircumference() const{
	return PI * r * 2;
}

double Circle::calculateArea() const{
	return PI * r * r;
}

bool Circle::operator==(Circle &c5) {
	if (this->r == c5.r)
		return true;
	else
		return false;
}
//}
